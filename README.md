# robo_control

In order to be able to run any of these make sure you have the latest stable version of node and vue installed.

## Environment configuration

This front-end application requires the configuration of some environment variables to function correctly. Instructions for setting this up are as follows:

1. Create a file named `.env.local` in the project root
2. Add the following contents to the file
```
VUE_APP_ROBO_HOST           = <(local) url of the control panel back-end>
VUE_APP_GITLAB_HOST         = <gitlab host here>
VUE_APP_GITLAB_ID           = <gitlab application id here>
VUE_APP_GITLAB_SECRET       = <gitlab application secret here>
VUE_APP_GITLAB_CALLBACK_URI = <(local) url of the control panel front-end>gitlab-auth
```

For the back-end we currently use a mockup server, and hence that url will likely be `http://localhost:8081/`. For the control panel front-end url, when developing, this will likely be `http://localhost:8080/`.

Note that the URLs for `ROBO_HOST` and `GITLAB_HOST` should end with `/`, as they're base URLs to which various paths are appended in the code.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Running issues
If you get issues with workers while serving or building check out
 [this link](https://github.com/gatsbyjs/gatsby/issues/11406)