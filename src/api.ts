import axios from 'axios';

/**
 * This is the api wrapper, designed as a central entry point for our app.
 */
class FrontEndAPI {
  private static instance: FrontEndAPI

  constructor() {
    if (!FrontEndAPI.instance) {
      FrontEndAPI.instance = this;
    }

    return FrontEndAPI.instance;
  }

  // Officer back-end host
  host = process.env.VUE_APP_ROBO_HOST;

  // Gitlab host address
  gitlabHost = process.env.VUE_APP_GITLAB_HOST;

  // Gitlab id required for OAuth flow
  gitlabId = process.env.VUE_APP_GITLAB_ID;

  // Gitlab secret required for OAuth flow
  gitlabSecret = process.env.VUE_APP_GITLAB_SECRET;

  // Gitlab Instance Host
  gitlabOauthUri = `${this.gitlabHost}oauth/authorize`;

  // Gitlab token host
  gitlabTokenUri = `${this.gitlabHost}oauth/redirect`;

  // Call back URL to be used by gitlab when returning
  callbackUri = process.env.VUE_APP_GITLAB_CALLBACK_URI;

  // Officer back-end proxy for OAuth info
  oauthValidationBaseUri = `${this.host}oauth/token/info`;

  /**
   * Returns a json containing the a page of repositories.
   *
   * @param page       The number of the page.
   * @param size       The size of the page.
   * @param autoFill   If to use the autofill function of the backend.
   */
  getRepositories(page: number, size: number, autoFill: boolean) {
    return this.requestOnPath(`repository/list?pageNo=${page}`
      + `&autoFill=${autoFill}&pageSize=${size}`);
  }

  /**
   * Returns a json containing the default settings.
   */
  getDefaultSettings() {
    return this.requestOnPath('settings/default');
  }

  /**
   * Returns a repository containing the settings of a certain repository.
   *
   * @param id the id of the repository
   */
  getRepositorySettings(id: number) {
    return this.requestOnPath(`settings/repository/${id}`);
  }

  /**
   * Sends a PUT request containing the settings for a repository.
   *
   * @param id the id of the repository
   * @param settings the settings of the repository in json format
   */
  putRepositorySettings(id: number, settings: undefined) {
    return axios({
      method: 'put',
      url: `${this.host}settings/repository/${id}`,
      data: settings,
    });
  }

  /**
   * Sends a PUT request containing the default settings.
   *
   * @param settings the default settings in json format
   */
  putDefaultSettings(settings: {}) {
    return axios({
      method: 'put',
      url: `${this.host}settings/default`,
      data: settings,
    });
  }

  /**
   * Sends a GET request on a certain path.
   *
   * @param path the path to be queried
   */
  requestOnPath(path: string) {
    return axios.get(`${this.host}${path}`);
  }

  /**
   * Request the OAuth token from the Gitlab server using the earlier requested code, which is
   * passed as an argument.
   *
   * @param code the OAuth request code used to get a valid OAuth token
   */
  requestAuthToken(code: string) {
    // TODO: This will not work before we let the robo risk officer java app handle it.
    return axios.post(`${this.gitlabTokenUri}#client_id=${this.gitlabId}&client_secret=${this.gitlabSecret}&code=${code}&grant_type=authorization_code&redirect_uri=${this.callbackUri}`);
  }

  /**
   * Redirects the user to the correct GitLab OAuth point, which will redirect the user back
   * to `/gitlab-auth` as part of the OAuth authentication flow.
   */
  requestAuthRedirect(secret: string) {
    window.location.replace(`${this.gitlabOauthUri}?client_id=${this.gitlabId}&redirect_uri=${this.callbackUri}&response_type=token&state=${secret}&scope=api`);
  }

  /**
   * Returns whether the given token is a valid GitLab OAuth token.
   *
   * @param token the token to verify
   */
  async verifyAuthToken(token: string) {
    // TODO: When the RRO java app supports authentication catching, then we want to
    //       set default headers etc.

    // Temporary validity check for now.
    try {
      await axios.get(`${this.oauthValidationBaseUri}?access_token=${token}`);
      axios.defaults.headers.Authorization = `Bearer ${token}`;
      return true;
    } catch (error) {
      return false;
    }
  }
}

const instance = new FrontEndAPI();

export default instance;
