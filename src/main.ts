import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import vuetify from './plugins/vuetify';

// Prevent getting production tip on vue startup
Vue.config.productionTip = false;

/**
 * Create a new vue instance. This is the entry point of the front end application.
 * The router is our router in ./router, which handles the routes.
 * Vuetify is the UI library we use.
  */
new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
