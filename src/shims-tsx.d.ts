import Vue, { VNode } from 'vue';

/*
This file allows us to use .tsx files while enabling jsx syntax support
in our IDE to write JSX-style typescript code.
 */
declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode {}
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}
