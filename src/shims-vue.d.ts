/*
This file is used to help our IDE understand what a file ending in .vue is.
Source: https://stackoverflow.com/questions/54622621/what-does-the-shims-tsx-d-ts-file-do-in-a-vue-typescript-project
 */
declare module '*.vue' {
  import Vue from 'vue';
}
