import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import api from '@/api';
import axios from 'axios';
import Login from '../views/Login.vue';
import Auth from '../views/Auth.vue';
import Repositories from '../views/Repositories.vue';

Vue.use(VueRouter);

/*
This is the main router. We can define all our routes here or include sub routers.
 */
const routes: Array<RouteConfig> = [
  {
    /**
     * The root path of our webapp is / and it loads the Home component.
     */
    path: '/',
    name: 'Home',
    component: Repositories,
    meta: {
      requiresAuth: true,
    },
  },
  {
    /**
     * The settings path of our webapp is /settings, it loads the Settings component.
     */
    path: '/settings',
    name: 'Settings',
    component: () => import('../views/Settings.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    /**
     * The login path of our webapp is /login, it loads the Login component.
     */
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    /**
     * The Authentication path of our webapp is /gitlab-auth, it loads the Auth component.
     */
    path: '/gitlab-auth',
    name: 'Auth',
    component: Auth,
  },
  {
    /**
     * The repository settings path of our webapp is /settings/:id,
     * it loads the RepoSettings view.
     */
    name: 'RepoSettings',
    path: '/settings/:id',
    component: () => import('../views/RepoSettings.vue'),
    props: true,
    meta: {
      requiresAuth: true,
    },
  },
];

// Create the router
const router = new VueRouter({
  mode: 'history',
  routes,
});

/**
 * Does authentication before routing.
 */
router.beforeEach(async (to, from, next) => {
  // Check if current token is valid
  const isValid = await api.verifyAuthToken(localStorage.getItem('token') || '');

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // Redirect (and logout) user if token is not valid
    if (!localStorage.getItem('token') || !isValid) {
      localStorage.removeItem('token');
      delete axios.defaults.headers.Authorization;
      window.dispatchEvent(new CustomEvent('user-logged-out'));
      next({
        path: '/login',
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
