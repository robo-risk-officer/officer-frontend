import Vue from 'vue';
import Vuetify from 'vuetify/lib';

/*
This lets our app use Vuetify.
Vuetify is the main UI framework we use on top of vue.
 */
Vue.use(Vuetify);

export default new Vuetify({
});
